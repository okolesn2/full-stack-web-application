//PART H

package com.example.D288BackEndProgramming.controllers;
import com.example.D288BackEndProgramming.services.CheckoutInterfaceService;
import com.example.D288BackEndProgramming.services.PurchaseDTO;
import com.example.D288BackEndProgramming.services.PurchaseResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


//Write code for the controllers package that includes a REST controller checkout controller class with a post mapping to place orders
@RestController
@RequestMapping(path ="api/checkout")
@CrossOrigin("http://localhost:4200")
public class CheckoutController {

    private final CheckoutInterfaceService checkoutInterfaceService;

    @Autowired
    public CheckoutController(CheckoutInterfaceService checkoutInterfaceService){
        this.checkoutInterfaceService= checkoutInterfaceService;
    }

    @PostMapping("/purchase")
    public PurchaseResponseDTO placeOrder(@RequestBody PurchaseDTO purchaseDTO){
        return checkoutInterfaceService.placeOrder(purchaseDTO);
    }

}
