package com.example.D288BackEndProgramming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class D288BackEndProgrammingApplication {

	public static void main(String[] args) {
		SpringApplication.run(D288BackEndProgrammingApplication.class, args);
	}

}
