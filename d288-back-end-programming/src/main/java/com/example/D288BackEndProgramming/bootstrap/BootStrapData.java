//PART I

package com.example.D288BackEndProgramming.bootstrap;
import com.example.D288BackEndProgramming.dao.CustomerRepository;
import com.example.D288BackEndProgramming.dao.DivisionRepository;
import com.example.D288BackEndProgramming.entities.Customer;
import com.example.D288BackEndProgramming.entities.Division;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;


//
@Component
public class BootStrapData implements CommandLineRunner {

    
    private final CustomerRepository customerRepository;


    public BootStrapData (CustomerRepository customerRepository, DivisionRepository divisionRepository){
        this.customerRepository = customerRepository;
    }

    @Override
    public void run (String... args) throws Exception{


        Division div1 = new Division(14L);
        Customer bob = new Customer("Bob","Smith","1234 Ice Court","12345","(123)123-1234",div1 );
        customerRepository.save(bob);

        Division div2 = new Division(12L);
        Customer jane = new Customer("Jane","Kale","5678 Liquid Court","5432","(234)534-4567",div2);
        customerRepository.save(jane);

        Division div3 = new Division(11L);
        Customer eric = new Customer("Eric","Fow","6544 Gas Court","8765","(234)765-9876",div3);
        customerRepository.save(eric);

        Division div4 = new Division(22L);
        Customer karl = new Customer("Karl","Gall","4567 Plasma Court","1346","(765)234-6455",div4);
        customerRepository.save(karl);

        Division div5 = new Division(13L);
        Customer rick = new Customer("Rick","Sand","7498 Matter Court","1844","(835)582-3456",div5);
        customerRepository.save(rick);



    }
}
