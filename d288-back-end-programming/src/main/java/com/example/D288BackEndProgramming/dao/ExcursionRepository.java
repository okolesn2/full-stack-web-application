//Part E

package com.example.D288BackEndProgramming.dao;
import com.example.D288BackEndProgramming.entities.Excursion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

//@Repository // Annotation is optional since the interface extends JpaRepository
//@RepositoryRestResource(collectionResourceRel = "excursions" ,path ="excursions" )// collectionResourceRel and path are optional since "excursions" is already the default, however useful if the endpoints change
@CrossOrigin(origins ="http://localhost:4200/")
public interface ExcursionRepository extends JpaRepository<Excursion,Long> {
}
