//Part E

package com.example.D288BackEndProgramming.dao;
import com.example.D288BackEndProgramming.entities.Vacation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

//s@Repository // Annotation is optional since the interface extends JpaRepository
//@RepositoryRestResource(collectionResourceRel = "vacations" ,path ="vacations" )// collectionResourceRel and path are optional since "vacations" is already the default, however useful if the endpoints change
@CrossOrigin(origins ="http://localhost:4200/")
public interface VacationRepository extends JpaRepository<Vacation,Long> {
}
