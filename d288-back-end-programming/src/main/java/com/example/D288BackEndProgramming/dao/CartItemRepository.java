//Part E

package com.example.D288BackEndProgramming.dao;
import com.example.D288BackEndProgramming.entities.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

//@Repository // Annotation is optional since the interface extends JpaRepository
//@RepositoryRestResource(collectionResourceRel = "cartItems" ,path ="cartItems" )// collectionResourceRel and path are optional since "cartItems" is already the default, however useful if the endpoints change
@CrossOrigin("http://localhost:4200/")
public interface CartItemRepository extends JpaRepository<CartItem,Long > {
}
