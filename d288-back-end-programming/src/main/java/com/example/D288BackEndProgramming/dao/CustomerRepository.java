//Part E

package com.example.D288BackEndProgramming.dao;
import com.example.D288BackEndProgramming.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

//@Repository // Annotation is optional since the interface extends JpaRepository
//@RepositoryRestResource(collectionResourceRel = "customers" ,path ="customers" )// collectionResourceRel and path are optional since "customers" is already the default, however useful if the endpoints change
@CrossOrigin(origins ="http://localhost:4200/")
public interface CustomerRepository extends JpaRepository<Customer,Long> {



}
