//Part E

package com.example.D288BackEndProgramming.dao;
import com.example.D288BackEndProgramming.entities.Division;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

//@Repository // Annotation is optional since the interface extends JpaRepository
//@RepositoryRestResource(collectionResourceRel = "divisions" ,path ="divisions" )// collectionResourceRel and path are optional since "divisions" is already the default, however useful if the endpoints change
@CrossOrigin(origins ="http://localhost:4200/")
public interface DivisionRepository extends JpaRepository<Division,Long> {
}
