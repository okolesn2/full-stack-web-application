//Part E

package com.example.D288BackEndProgramming.dao;
import com.example.D288BackEndProgramming.entities.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

//@Repository // Annotation is optional if interface extends JpaRepository
//@RepositoryRestResource( collectionResourceRel = "countries" ,path ="countries" ) // collectionResourceRel and path are optional since "countries" is already the default, however useful if the endpoints change
@CrossOrigin(origins ="http://localhost:4200/")
public interface CountryRepository extends JpaRepository<Country,Long> {
}
