// Part D

package com.example.D288BackEndProgramming.entities;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
@JsonAutoDetect
@Entity
@Getter(AccessLevel.PUBLIC)//Note:Legal access levels are PUBLIC, PROTECTED, PACKAGE, and PRIVATE
@Setter(AccessLevel.PUBLIC)
@NoArgsConstructor
@Table (name = "customers")
public class Customer implements Serializable {

    //Primary Key
    @jakarta.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "customer_id")
    private long id;


    //Attributes
    @Column(name = "customer_first_name",nullable = false)
    private String firstName;

    @Column(name = "customer_last_name",nullable = false)
    private String lastName;

    @Column(name = "address",nullable = false)
    private String address;

    @Column(name = "postal_code",nullable = false)
    private String postal_code;

    @Column(name = "phone",nullable = false)
    private String phone;

    @Column(name = "create_date",nullable = false,updatable =false)
    @CreationTimestamp
    private Date create_date;

    @Column(name = "last_update",nullable = false)
    @UpdateTimestamp
    private Date last_update;


    //Customer has a Many-To-One Unidirectional Association with Division
    @ManyToOne()
    @JoinColumn(name = "division_id",referencedColumnName = "division_id" ,updatable =false)
    private Division division;

    //Customer has a One-to-Many Bidirectional association with cart
    @OneToMany(mappedBy = "customer",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private Set<Cart> carts = new HashSet<>();


    public void add(Cart item) {

        if (item != null){
            if(carts == null){
                carts = new HashSet<>();
            }
            carts.add(item);
            item.setCustomer(this);
        }
    }


    //Custom Constructor
    public Customer (String firstName, String lastName, String address ,String postal_code, String phone,Division division ){
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.postal_code = postal_code;
        this.phone = phone;
        this.setDivision(division);

    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' +
                ", postal_code='" + postal_code + '\'' +
                ", phone='" + phone + '\'' +
                ", create_date=" + create_date +
                ", last_update=" + last_update +
                ", division=" + division +
                ", carts=" + carts +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        return id == customer.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }


}
