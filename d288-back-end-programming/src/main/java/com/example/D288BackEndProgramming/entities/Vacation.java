// Part D

package com.example.D288BackEndProgramming.entities;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
@JsonAutoDetect
@Entity
@Getter(AccessLevel.PUBLIC)//Note:Legal access levels are PUBLIC, PROTECTED, PACKAGE, and PRIVATE
@Setter(AccessLevel.PUBLIC)
@NoArgsConstructor
@Table (name = "vacations")
public class Vacation implements Serializable {

    //Primary Key
    @jakarta.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "vacation_id") //Matches Database Schema
    private Long id; //Matches Front-End


    //Attributes
    @Column(name = "vacation_title")//Matches Database Schema
    private String vacation_title; //Matches Front-End

    @Column(name = "description")//Matches Database Schema
    private String description;//Matches Front-End

    @Column(name = "travel_fare_price")//Matches Database Schema
    private BigDecimal travel_price;//Matches Front-End

    @Column(name = "image_url")//Matches Database Schema
    private String image_URL;//Matches Front-End

    @Column(name = "create_date")//Matches Database Schema
    @CreationTimestamp
    private Date create_date;//Matches Front-End

    @Column(name = "last_update")//Matches Database Schema
    @UpdateTimestamp
    private Date last_update;//Matches Front-End

    // No Foreign Key

    //Vacation has a Bidirectional Many-to-One relationship with Excursion
    @OneToMany(mappedBy = "vacation",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private Set<Excursion> excursions = new HashSet<>();//Matches Front-End



    @Override
    public String toString() {
        return "Vacation{" +
                "id=" + id +
                ", vacation_title='" + vacation_title + '\'' +
                ", description='" + description + '\'' +
                ", travel_price=" + travel_price +
                ", image_URL='" + image_URL + '\'' +
                ", create_date=" + create_date +
                ", last_update=" + last_update +
                ", excursions=" + excursions +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vacation vacation = (Vacation) o;

        return Objects.equals(id, vacation.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
