// Part D

package com.example.D288BackEndProgramming.entities;


import lombok.Getter;

@Getter
public enum StatusType {
    pending,
    ordered,
    cancelled
}
