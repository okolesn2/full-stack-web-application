// Part D

package com.example.D288BackEndProgramming.entities;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
@JsonAutoDetect
@Entity
@Getter(AccessLevel.PUBLIC)//Note:Legal access levels are PUBLIC, PROTECTED, PACKAGE, and PRIVATE
@Setter(AccessLevel.PUBLIC)
@NoArgsConstructor
@Table (name = "excursions")//Matches Database Schema
public class Excursion implements Serializable {

    //Primary Key
    @jakarta.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "excursion_id")
    private Long id;


    //Attributes
    @Column(name = "excursion_title")//Matches Database Schema
    private String excursion_title;

    @Column(name = "excursion_price")//Matches Database Schema
    private BigDecimal excursion_price;

    @Column(name = "image_url")//Matches Database Schema
    private String image_URL;

    @Column(name = "create_date")//Matches Database Schema
    @CreationTimestamp
    private Date create_date;

    @Column(name = "last_update")//Matches Database Schema
    @UpdateTimestamp
    private Date laste_update;


    //Excursion has a Bidirectional Many-to-One relationship with vacation
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "vacation_id",referencedColumnName = "vacation_id",nullable = false)//Matches Database Schema
    private Vacation vacation;


    //Excursions has a Bidirectional Many-to-Many association with CartItems
    @ManyToMany (mappedBy = "excursions",fetch = FetchType.LAZY)
    private Set<CartItem> cartItems  = new HashSet<>();

    @Override
    public String toString() {
        return "Excursion{" +
                "id=" + id +
                ", excursion_title='" + excursion_title + '\'' +
                ", excursion_price=" + excursion_price +
                ", image_URL='" + image_URL + '\'' +
                ", create_date=" + create_date +
                ", laste_update=" + laste_update +
                ", vacation=" + vacation +
                ", cartItems=" + cartItems +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Excursion excursion = (Excursion) o;

        return Objects.equals(id, excursion.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
