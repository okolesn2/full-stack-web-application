// Part D

package com.example.D288BackEndProgramming.entities;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.Id;
import java.io.Serializable;
import java.util.Date;


@JsonAutoDetect
@Entity
@Getter(AccessLevel.PUBLIC)//Note:Legal access levels are PUBLIC, PROTECTED, PACKAGE, and PRIVATE
@Setter(AccessLevel.PUBLIC)
@NoArgsConstructor
@Table (name = "divisions")

public class Division implements Serializable {

    //Primary Key
    @jakarta.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "division_id")//Matches Database Schema
    private long id; //Matches Front-End


    //Attributes
    @Column(name = "division") //Matches Database Schema
    private String division_name;//Matches Front-End

    @Column(name = "create_date")//Matches Database Schema
    @CreationTimestamp
    private Date create_date;

    @Column(name = "last_update")//Matches Database Schema
    @UpdateTimestamp
    private Date last_update;



    //Division has a Bidirectional Many-to-One relationship with Country
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id",referencedColumnName = "country_id",nullable = false,insertable =false, updatable =false)
    private Country country ;



    //Populates division in the add customer page
    @Column(name = "country_id")
    private long country_id;

    public void setCountry(Country country){
        setCountry_id(country.getId());
        this.country =country;
    }


    //Custom constructor
    public Division (Long division_id ){

        this.id = division_id;
    }

    @Override
    public String toString() {
        return "Division{" +
                "id=" + id +
                ", division_name='" + division_name + '\'' +
                ", create_date=" + create_date +
                ", last_update=" + last_update +
                ", country=" + country +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Division division = (Division) o;

        return id == division.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
