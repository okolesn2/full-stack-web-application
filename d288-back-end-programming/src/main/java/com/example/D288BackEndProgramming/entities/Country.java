// Part D

package com.example.D288BackEndProgramming.entities;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
@JsonAutoDetect
@Entity
@Getter(AccessLevel.PUBLIC)//Note:Legal access levels are PUBLIC, PROTECTED, PACKAGE, and PRIVATE
@Setter(AccessLevel.PUBLIC)
@NoArgsConstructor
@Table (name = "countries" )//Matches Database Schema
public class Country implements Serializable {

    //Primary Key
    @jakarta.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "country_id") //Matches Database Schema
    private long id;//Matches Front-End


    //Attributes
    @Column(name = "country") //Matches Database Schema
    private String country_name;//Matches Front-End

    @Column(name = "create_date") //Matches Database Schema
    @CreationTimestamp
    private Date create_date;

    @Column(name = "last_update") //Matches Database Schema
    @UpdateTimestamp
    private Date last_update;


    // No Foreign Key

    //Bidirectional One-to-Many/Many-to-One relationship  with Division
    @OneToMany(mappedBy = "country",cascade = CascadeType.ALL,fetch = FetchType.LAZY) //ER Diagram
    private Set<Division> divisions = new HashSet<>();

    @Override
    public String toString() {
        return "Country{" +
                "id=" + id +
                ", country_name='" + country_name + '\'' +
                ", create_date=" + create_date +
                ", last_update=" + last_update +
                ", divisions=" + divisions +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Country country = (Country) o;

        return id == country.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
