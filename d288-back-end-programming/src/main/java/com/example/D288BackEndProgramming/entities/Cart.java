// Part D

package com.example.D288BackEndProgramming.entities;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
@JsonAutoDetect
@Entity
@Getter(AccessLevel.PUBLIC)//Note:Legal access levels are PUBLIC, PROTECTED, PACKAGE, and PRIVATE
@Setter(AccessLevel.PUBLIC)
@NoArgsConstructor
@Table (name = "carts")
public class Cart implements Serializable {

    //Primary Key
    @jakarta.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "cart_id")//Matches Database Schema
    private long id;


    //Attributes
    @Column(name = "order_tracking_number")
    private String orderTrackingNumber;

    @Column(name = "package_price")//Matches Database Schema
    private BigDecimal package_price;

    @Column(name = "party_size")//Matches Database Schema
    private int party_size;

    @Column(name = "create_date")//Matches Database Schema
    @CreationTimestamp
    private Date create_date;

    @Column(name = "last_update")//Matches Database Schema
    @UpdateTimestamp
    private Date last_update;

    //Mapping Enums as String Value
    @Enumerated(EnumType.STRING)
    @Column(name = "status",nullable = false)//Matches Database Schema
    private StatusType status;


    //Cart has a Bidirectional Many-to-One association with Customer
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id",referencedColumnName = "customer_id")//Matches Database Schema
    private Customer customer;


    //Cart has a One-to-Many Bidirectional association with CartItem
    @OneToMany(mappedBy = "cart",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private Set<CartItem> cartItem = new HashSet<>();


    @Override
    public String toString() {
        return "Cart{" +
                "id=" + id +
                ", orderTrackingNumber='" + orderTrackingNumber + '\'' +
                ", package_price=" + package_price +
                ", party_size=" + party_size +
                ", create_date=" + create_date +
                ", last_update=" + last_update +
                ", status=" + status +
                ", customer=" + customer +
                ", cartItem=" + cartItem +
                '}';
    }

    //This Method used in CheckoutImplementationService.java file in a lambda function
    public void add (CartItem item){
        if (item != null){
            if(cartItem == null){
                cartItem = new HashSet<>();
            }
            cartItem.add(item);
            item.setCart(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cart cart = (Cart) o;

        return id == cart.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
