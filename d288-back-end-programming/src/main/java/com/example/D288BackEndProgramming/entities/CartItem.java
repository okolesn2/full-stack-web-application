// Part D

package com.example.D288BackEndProgramming.entities;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
@JsonAutoDetect
@Entity
@Getter(AccessLevel.PUBLIC)//Note:Legal access levels are PUBLIC, PROTECTED, PACKAGE, and PRIVATE
@Setter(AccessLevel.PUBLIC)
@NoArgsConstructor
@Table (name = "cart_items")
public class CartItem implements Serializable {

    //Primary Key
    @jakarta.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "cart_item_id")
    private long id;


    //Attributes
    @Column(name = "create_date")
    @CreationTimestamp
    private Date create_date;

    @Column(name = "last_update")
    @UpdateTimestamp
    private Date last_update;


    //CartItem has a Bidirectional Many-to-One association with Cart
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "cart_id")
    private Cart cart;

    //CartItem has a Unidirectional Many-to-One association with Vacation
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "vacation_id")
    private Vacation vacation;


    //CartItem has a Bidirectional Many-to-Many association with Excursions.
    @ManyToMany
    @JoinTable(
            name = "excursion_cartitem",
            joinColumns = @JoinColumn(name = "cart_item_id",referencedColumnName = "cart_item_id",nullable = false),
            inverseJoinColumns = @JoinColumn(name = "excursion_id",referencedColumnName = "excursion_id",nullable = false))
    private Set<Excursion> excursions = new HashSet<>();

    @Override
    public String toString() {
        return "CartItem{" +
                "id=" + id +
                ", create_date=" + create_date +
                ", last_update=" + last_update +
                ", cart=" + cart +
                ", vacation=" + vacation +
                ", excursions=" + excursions +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CartItem cartItem = (CartItem) o;

        return id == cartItem.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
