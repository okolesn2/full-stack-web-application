//Part F

package com.example.D288BackEndProgramming.services;
import com.example.D288BackEndProgramming.dao.CustomerRepository;
import com.example.D288BackEndProgramming.entities.Cart;
import com.example.D288BackEndProgramming.entities.CartItem;
import com.example.D288BackEndProgramming.entities.Customer;
import com.example.D288BackEndProgramming.entities.StatusType;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.UUID;

@Service
public class CheckoutImplementationService implements CheckoutInterfaceService {

    //Initializing customerRepository
    private final CustomerRepository customerRepository;


    @Autowired //optional since there is only one constructor
    //
    public CheckoutImplementationService(CustomerRepository customerRepository){
        this.customerRepository=customerRepository;
    }
    @Override
    @Transactional
    public PurchaseResponseDTO placeOrder(PurchaseDTO purchaseDTO) {
        //retrieve purchase info from dto
        Cart cart = purchaseDTO.getCart();

        //generate tracking number
        String orderTrackingNumber = generateOrderTrackingNumber();
        cart.setOrderTrackingNumber(orderTrackingNumber);

        //Updates the statusType to ordered once an order is placed.
        cart.setStatus(StatusType.ordered);

        //populate cart with cartItems
        Set<CartItem> cartItems = purchaseDTO.getCartItems();
        cartItems.forEach(cart::add); //same as L cartItems.forEach(item ->cart.add(item));

        //populate customer with cart
        Customer customer = purchaseDTO.getCustomer();
        customer.add(cart);

        //save to the database
        customerRepository.save(customer);

        //return a response
        return new PurchaseResponseDTO(orderTrackingNumber);



    }

    private String generateOrderTrackingNumber() {
        //Generate a random Universally Unique Identifier (UUID) number (version 4)
        return UUID.randomUUID().toString();
    }
}
