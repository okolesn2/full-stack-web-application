//Part F

package com.example.D288BackEndProgramming.services;
import lombok.Data;
import lombok.NonNull;


//This DTO sends back a java object as JSON
@Data// adds getter,setter and constructor methods
@NonNull
public class PurchaseResponseDTO {

    private final String orderTrackingNumber;
}
