//Part F

package com.example.D288BackEndProgramming.services;


//a checkout service interface
public interface CheckoutInterfaceService {

    PurchaseResponseDTO placeOrder (PurchaseDTO purchaseDTO);
}
