//Part F

package com.example.D288BackEndProgramming.services;
import com.example.D288BackEndProgramming.entities.Cart;
import com.example.D288BackEndProgramming.entities.CartItem;
import com.example.D288BackEndProgramming.entities.Customer;
import lombok.Data;

import java.util.Set;


//This is a Data Transfer Object (DTO)
@Data// adds getter,setter and constructor methods
public class PurchaseDTO {

    private Customer customer;
    private Cart cart;
    private Set<CartItem> cartItems;//Note: A set is used here , but it is converted to an array when submitted as a JSON file over HTTP

}


